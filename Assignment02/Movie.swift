//
//  Movie.swift
//  Assignment02
//
//  Created by Saleh Sultan on 10/4/18.
//  Copyright © 2018 SwordfishTech. All rights reserved.
//

import Cocoa

class Movie: NSObject {
    public static var REGULAR = 0
    public static var NEW_RELEASE = 1
    public static var CHILDREN = 2
    
    private var _title:String
    private var _priceCode: Int
    
    public init(title: String, priceCode:Int) {
        _title = title
        _priceCode = priceCode
    }
    
    public func getPriceCode() -> Int {
        return _priceCode
    }
    
    public func setPriceCode(price:Int) {
        _priceCode = price
    }
    
    public func getTitle() -> String {
        return _title
    }
}
