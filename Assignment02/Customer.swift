//
//  Customer.swift
//  Assignment02
//
//  Created by Saleh Sultan on 10/4/18.
//  Copyright © 2018 SwordfishTech. All rights reserved.
//

import Cocoa

public class Customer: NSObject {
    
    private var _name:String
    private var _rentals = [Rental]()
    
    init(name: String) {
        _name = name
    }
    
    func addRental(rental: Rental) {
        _rentals.append(rental)
    }
    
    public func getName() -> String {
        return _name
    }
    
    fileprivate func statementHeader(_ name: String) -> String {
        return "Rental Record for " + name + "\n"
    }
    
    fileprivate func rentalDetails(forMovie title: String, withAmount amt:Double) -> String {
        return "\t \(title) \t \(amt)\n"
    }
    
    fileprivate func getAmount(_ each: Rental) -> Double{
        var thisAmount:Double = 0
        
        switch (each.getMovie().getPriceCode()) {
        case Movie.REGULAR:
            thisAmount += 2;
            if each.getDaysRented() > 2 {
                thisAmount += Double(each.getDaysRented() - 2) * 1.5
            }
            break
        case Movie.NEW_RELEASE:
            thisAmount += Double(each.getDaysRented() * 3)
            break
        case Movie.CHILDREN:
            thisAmount += 1.5
            if each.getDaysRented() > 3 {
                thisAmount += Double(each.getDaysRented() - 3) * 1.5
            }
            break
        default:
            break
        }
        return thisAmount
    }
    
    fileprivate func getFrequentRentalPoint() -> Int {
        var frequentRenterPoints = 0
        
        for each in _rentals {
            frequentRenterPoints += 1
            
            if each.getMovie().getPriceCode() == Movie.NEW_RELEASE && each.getDaysRented() > 1 {
                frequentRenterPoints += 1
            }
        }
        return frequentRenterPoints
    }
    
    public func statement() -> String {
        var totalAmount:Double = 0
        
        let name = getName()
        var result = statementHeader(name)
        
        for each in _rentals {
            let thisAmount = getAmount(each)
            result += rentalDetails(forMovie: each.getMovie().getTitle(), withAmount: thisAmount)
            totalAmount += thisAmount;
        }
        
        let frequentRenterPoints = getFrequentRentalPoint()
        return result + footer(forTotalAmt:totalAmount, andFrequentRenterPoints: frequentRenterPoints)
    }
    
    
    fileprivate func footer(forTotalAmt totalAmount: Double, andFrequentRenterPoints frp: Int) -> String{
        return "Amount owed is \(totalAmount) \nYou earned \(frp) frequent renter points"
    }
}
