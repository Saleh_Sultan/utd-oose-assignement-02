//
//  Rental.swift
//  Assignment02
//
//  Created by Saleh Sultan on 10/4/18.
//  Copyright © 2018 SwordfishTech. All rights reserved.
//

import Cocoa

class Rental: NSObject {
    private var _movie: Movie
    private var _daysRented: Int
    
    public init(movie: Movie, daysRented: Int) {
        _movie = movie
        _daysRented = daysRented
    }
    
    public func getDaysRented() -> Int {
        return _daysRented
    }
    
    public func getMovie() -> Movie {
        return _movie
    }
}
