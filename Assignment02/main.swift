//
//  main.swift
//  Assignment02
//
//  Created by Saleh Sultan on 10/4/18.
//  Copyright © 2018 SwordfishTech. All rights reserved.
//

import Foundation

var cs1 = Customer(name: "Saleh");
cs1.addRental(rental: Rental(movie: Movie(title: "Titanic", priceCode: Movie.REGULAR), daysRented: 2))
cs1.addRental(rental: Rental(movie: Movie(title: "Mission Impossible", priceCode: Movie.NEW_RELEASE), daysRented: 2))
cs1.addRental(rental: Rental(movie: Movie(title: "Jurassic World", priceCode: Movie.CHILDREN), daysRented: 4))
print(cs1.statement() + "\n\n")

var cs2 = Customer(name: "Sultan");
cs2.addRental(rental: Rental(movie: Movie(title: "Forest Gum", priceCode: Movie.REGULAR), daysRented: 2))
cs2.addRental(rental: Rental(movie: Movie(title: "Die Hard", priceCode: Movie.NEW_RELEASE), daysRented: 5))
cs2.addRental(rental: Rental(movie: Movie(title: "Minion", priceCode: Movie.CHILDREN), daysRented: 7))
print(cs2.statement() + "\n")
